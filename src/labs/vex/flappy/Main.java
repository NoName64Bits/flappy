package labs.vex.flappy;

import labs.vex.flappy.graphics.Shader;
import labs.vex.flappy.input.Input;
import labs.vex.flappy.level.Level;
import labs.vex.flappy.maths.Matrix4f;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Main implements Runnable {

    private static final int width      =   1280;
    private static final int height     =   720;
    private static final String title   =   "Test game";

    private Thread game;
    private boolean running = false;

    private long window;

    private Level level;

    public void start() {
        this.running = true;
        this.game = new Thread(this, "Game");
        this.game.start();
    }

    private void init() {
        if(!glfwInit()) {
            throw new Error("Could not initialize GLFW!");
        }

        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
        this.window = glfwCreateWindow(width, height, title, NULL, NULL);
        GLFWVidMode videomode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(this.window, (videomode.width() - width) / 2, (videomode.height() - height) / 2);
        if(this.window == NULL) {
            throw new Error("Could not initialize window!");
        }

        glfwSetKeyCallback(this.window, new Input());

        glfwMakeContextCurrent(this.window);
        glfwShowWindow(this.window);
        GL.createCapabilities();

        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glEnable(GL_DEPTH_TEST);
        System.out.println("OpenGL: " + glGetString(GL_VERSION));

        Shader.loadAll();

        Shader.BG.enable();
        Matrix4f pr_matrix = Matrix4f.orthographic(-10.0f, 10.0f, -10.0f * 9.0f / 16.0f,   10.0f * 9.0f / 16.0f, -1.0f, 1.0f);
        Shader.BG.setUniform("pr_matrix", pr_matrix);
        Shader.BG.disable();

        level = new Level();
    }

    public void run() {
        this.init();
        while(this.running) {
            this.update();
            this.render();

            if(glfwWindowShouldClose(this.window)) {
                this.running = false;
            }
        }
    }

    private void update() {
        glfwPollEvents();
        if(Input.isKeyPressed(GLFW_KEY_SPACE)) {
            System.out.println("FLAP!");
        }
    }

    private void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        level.render();
        glfwSwapBuffers(this.window);
    }

    public static void main(String[] args) {
        new Main().start();
    }
}
