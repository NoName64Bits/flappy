package labs.vex.flappy.graphics;

import labs.vex.flappy.maths.Matrix4f;
import labs.vex.flappy.maths.Vector3f;
import labs.vex.flappy.utils.ShaderUtils;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL20.*;

public class Shader {
    public static final int VERTEX = 0;
    public static final int TEXTURE = 1;

    public static Shader BG;

    private final int id;
    private Map<String, Integer> uniforms = new HashMap<String, Integer>();

    public Shader(String name) {
        this.id = ShaderUtils.load(name);
    }

    public static void loadAll() {
        BG = new Shader("shaders/bg");
    }

    public int getUniformLocation(String name) {
        if(this.uniforms.containsKey(name)) {
            return this.uniforms.get(name);
        }

        int result = glGetUniformLocation(this.id, name);
        uniforms.put(name, result);
        if(result == -1) {
            System.err.println("Could not find uniform " + name);
        }
        return result;
    }

    public void setUniform(String name, int value) {
        glUniform1i(getUniformLocation(name), value);
    }

    public void setUniform(String name, float value) {
        glUniform1f(getUniformLocation(name), value);
    }

    public void setUniform(String name, float v1, float v2) {
        glUniform2f(getUniformLocation(name), v1, v2);
    }

    public void setUniform(String name, Vector3f value) {
        glUniform3f(getUniformLocation(name), value.x, value.y, value.z);
    }

    public void setUniform(String name, Matrix4f value) {
        glUniformMatrix4fv(getUniformLocation(name), false, value.toFloatBuffer());
    }

    public void enable() {
        glUseProgram(this.id);
    }

    public void disable() {

    }
}
