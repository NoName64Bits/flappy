package labs.vex.flappy.utils;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

public class ShaderUtils {

    private ShaderUtils() {}

    public static int load(String name) {
        String vertexName = name + ".vs";
        String fragmentName = name + ".fs";

        String vertex = FileUtils.loadAsString(vertexName);
        String fragment = FileUtils.loadAsString(fragmentName);

        return create(vertex, fragment);
    }

    public static int create(String vertex, String fragment) {
        int program = glCreateProgram();
        int vertexId = glCreateShader(GL_VERTEX_SHADER);
        int fragmentId = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(vertexId, vertex);
        glShaderSource(fragmentId, fragment);

        glCompileShader(vertexId);
        if(glGetShaderi(vertexId, GL_COMPILE_STATUS) == GL_FALSE) {
            System.err.println("Failed to compile Vertex Shader!");
            System.out.println(glGetShaderInfoLog(vertexId));
            return -1;
        }

        glCompileShader(fragmentId);
        if(glGetShaderi(fragmentId, GL_COMPILE_STATUS) == GL_FALSE) {
            System.err.println("Failed to compile Fragment Shader!");
            System.out.println(glGetShaderInfoLog(fragmentId));
            return -1;
        }

        glAttachShader(program, vertexId);
        glAttachShader(program, fragmentId);
        glLinkProgram(program);
        glValidateProgram(program);

        glDeleteShader(vertexId);
        glDeleteShader(fragmentId);

        return program;
    }

}
