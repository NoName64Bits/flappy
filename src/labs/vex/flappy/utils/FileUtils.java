package labs.vex.flappy.utils;

import java.io.BufferedReader;
import java.io.FileReader;

public class FileUtils {

    private FileUtils() {};

    public static String loadAsString(String file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String buffer;
            while((buffer = reader.readLine()) != null) {
                result.append(buffer + "\n");
            }
            reader.close();
            return result.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
